package vinhsan.uit.tesseractdemo;

import android.content.Intent;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.googlecode.tesseract.android.TessBaseAPI;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MainActivity extends AppCompatActivity {
    ImageView imgv;
    Button btnCam,btnChoose,btnConv;
    TextView txtv;
    int REQUEST_CODE_CAM=123;
    int REQUEST_CODE_CHOOSE=456;
    Uri uri;
    Bitmap bitmap;
    Bitmap bm;
    TessBaseAPI mTess;
    String datapath = "";
    static final String TAG = "YOUR-TAG-NAME";

    //private TessOCR mTessOCR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imgv = (ImageView) findViewById(R.id.imgv);
        btnCam = (Button) findViewById(R.id.btnCam);
        btnChoose = (Button) findViewById(R.id.btnChoose);
        btnConv = (Button) findViewById(R.id.btnConv);
        txtv = (TextView) findViewById(R.id.txtv);

        //imgv.setImageResource(R.drawable.uit);

        //bm = BitmapFactory.decodeResource(getResources(), R.drawable.abc);
        //TessBaseAPI mTess = new TessBaseAPI();
        //mTess.setImage(bm);
        //String result = mTess.getUTF8Text();
        //txtv.setText(result);

        //init image
        bm = BitmapFactory.decodeResource(getResources(), R.drawable.uit);

        //initialize Tesseract API
        String language = "eng";
        datapath = getFilesDir()+ "/tesseract/";
        mTess = new TessBaseAPI();

        checkFile(new File(datapath + "tessdata/"));

        mTess.init(datapath, language);

        btnCam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,REQUEST_CODE_CAM);

                //processImage();
            }
        });

        btnChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent,REQUEST_CODE_CHOOSE);

            }
        });

        btnConv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processImage();
            }
        });
    }

    public void processImage(){
        String OCRresult = null;
        mTess.setImage(bm);
        OCRresult = mTess.getUTF8Text();
        txtv = (TextView) findViewById(R.id.txtv);
        txtv.setText(OCRresult);
    }

    private void checkFile(File dir) {
        if (!dir.exists()&& dir.mkdirs()){
            copyFiles();
        }
        if(dir.exists()) {
            String datafilepath = datapath+ "/tessdata/eng.traineddata";
            File datafile = new File(datafilepath);

            if (!datafile.exists()) {
                copyFiles();
            }
        }
    }

    private void copyFiles() {
        try {
            String filepath = datapath + "/tessdata/eng.traineddata";
            AssetManager assetManager = getAssets();

            InputStream instream = assetManager.open("tessdata/eng.traineddata");
            OutputStream outstream = new FileOutputStream(filepath);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = instream.read(buffer)) != -1) {
                outstream.write(buffer, 0, read);
            }


            outstream.flush();
            outstream.close();
            instream.close();

            File file = new File(filepath);
            if (!file.exists()) {
                throw new FileNotFoundException();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_CAM && resultCode == RESULT_OK){
            bm = (Bitmap) data.getExtras().get("data");
            imgv.setImageBitmap(bm);
        }
        else if (requestCode == REQUEST_CODE_CHOOSE && resultCode == RESULT_OK){
            // Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            //imgv.setImageBitmap(bitmap);
            uri = data.getData();
            String[]projection = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(uri,projection,null,null,null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(projection[0]);
            String filePath = cursor.getString(columnIndex);
            cursor.close();

            bm = BitmapFactory.decodeFile(filePath);
            Drawable d = new BitmapDrawable(bm);

            imgv.setImageURI(uri);
            //imgv.setBackground(d);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}
